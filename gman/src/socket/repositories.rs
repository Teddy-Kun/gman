use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct Repositories {
	path: String,
	update: bool,
}

impl Default for Repositories {
	fn default() -> Self {
		Repositories {
			path: "/usr/gman/repositories/".to_string(),
			update: false,
		}
	}
}
