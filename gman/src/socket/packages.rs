use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
pub enum Operation {
	Install,
	Remove,
	Search,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Packages {
	packages: Vec<String>,
	operation: Operation,
}

impl Default for Packages {
    fn default() -> Self {
        Packages{
            operation: Operation::Search,
            packages: vec!["".to_string()]
        }
    }
}
