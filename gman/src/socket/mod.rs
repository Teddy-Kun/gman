use serde::{Deserialize, Serialize};

mod packages;
mod repositories;

pub mod listener;

#[derive(Serialize, Deserialize, Debug)]
pub enum Command {
	Repositories,
	Package,
}
