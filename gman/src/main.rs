mod args;
mod builder;
mod compress;
mod conf;
mod socket;

use builder::makefile;
use compress::{gzip, tar, zstd};
use conf::GmanConfig;
use socket::listener;

use std::{fs, process::exit};

fn main() {
	let args = args::get_args();

	if args.config == "/etc/gman/config.toml".to_string() {
		let res = fs::create_dir_all("/etc/gman/");
		if let Err(err) = res {
			println!("Error creating config file: {}", err)
		}
	}
	let c = GmanConfig::load(&args.config);
	if let Err(err) = c {
		println!("Error loading config file: {}", err);
		exit(1);
	}

	if args.gzip != "argument not set".to_string() {
		let res = gzip::decompress(&args.gzip);
		if let Err(err) = res {
			println!("Error decompressing gz file to ram: {}", err)
		}
	}

	if args.zstd != "argument not set".to_string() {
		if args.level > 9 {
			println!("Max compression level is 9")
		} else {
			let res = zstd::compress(&args.zstd, "out.zstd", args.level as i32);
			if let Err(err) = res {
				println!("Error compressing zstd file: {}", err)
			}

			let res = zstd::decompress_to_ram("out.zstd");
			if let Err(err) = res {
				println!("Error decompressing zstd file to ram: {}", err)
			}
		}
	}

	if args.tar != "argument not set".to_string() {
		let res = tar::tar_folder(&args.tar, "out.tar");
		if let Err(err) = res {
			println!("Error taring file: {}", err)
		}

		let res = tar::untar("out.tar", "outs/tar");
		if let Err(err) = res {
			println!("Error untaring file: {}", err)
		}
	}

	if args.make != "argument not set".to_string() {
		let m = makefile::Makefile::default();
		println!("Default makefile:\n{:#?}", m)
	}

	let sock = listener::listen();
	if sock.is_err() {
		println!("{:?}", sock.err());
		exit(1);
	}

	exit(0);
}
