use std::{
	fs::File,
	io::{self, BufReader, Read},
};

use flate2::bufread::GzDecoder;

pub fn decompress(file_path: &str) -> io::Result<Vec<u8>> {
	let file = File::open(file_path)?;
	let buf = BufReader::new(file);

	let mut gz = GzDecoder::new(buf);

	let mut decompressed = Vec::new();
	gz.read_to_end(&mut decompressed)?;

	Ok(decompressed)
}
