use std::{
	fs::File,
	io::{self, BufReader, BufWriter},
};

use zstd::Decoder;

// This function compresses a file given the path to it as a string:
pub fn compress(input_path: &str, output_path: &str, level: i32) -> io::Result<BufWriter<File>> {
	let f = File::open(input_path)?;
	let o = File::create(output_path)?;
	let writer = io::BufWriter::new(o);
	let mut reader = io::BufReader::new(f);

	let mut encoder = zstd::stream::Encoder::new(writer, level).unwrap();
	io::copy(&mut reader, &mut encoder).unwrap();
	encoder.finish()
}

// This function decompresses a file given the path to it as a string
pub fn decompress(input_path: &str, output_path: &str) -> io::Result<()> {
	let f = File::open(input_path)?;
	let o = File::create(output_path)?;
	let writer = io::BufWriter::new(o);
	let reader = io::BufReader::new(f);

	zstd::stream::copy_decode(reader, writer)
}

pub fn decompress_to_ram(file_path: &str) -> io::Result<Vec<u8>> {
	let file = File::open(file_path)?;
	let reader = BufReader::new(file);

	let mut decoder = Decoder::new(reader)?;
	let mut output = Vec::new();
	io::copy(&mut decoder, &mut output)?;
	Ok(output)
}

#[cfg(test)]
mod test {
	use std::fs;

	use super::*;

	#[test]
	fn test_compression() {
		let res = fs::create_dir_all("tests/results");
		assert!(res.is_ok(), "Could not create test folder");

		let res = compress(
			"tests/pls_compress/compress_me.txt",
			"tests/results/compressed.zstd",
			9,
		);
		assert!(res.is_ok(), "Error compressing");

		let res = decompress(
			"tests/pls_compress/compressed.zstd",
			"tests/results/decompressed.txt",
		);
		assert!(res.is_ok(), "Error decompressing");

		let compressed_sha = sha256::try_digest("tests/pls_compress/compress_me.txt").unwrap();
		let uncompressed_sha = sha256::try_digest("tests/results/decompressed.txt").unwrap();

		assert_eq!(compressed_sha, uncompressed_sha)
	}
}
