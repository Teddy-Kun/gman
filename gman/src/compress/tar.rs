use std::{
	collections::HashMap,
	fs::{self, File},
	io::{self, BufReader, Cursor, Read},
};

use tar::Archive;

pub fn tar_folder(source: &str, output_file: &str) -> io::Result<()> {
	let tar_gz = File::create(output_file)?;
	let mut tar = tar::Builder::new(tar_gz);

	tar.append_dir_all(".", source)?;

	tar.finish()?;
	Ok(())
}

pub fn untar(source: &str, destintation: &str) -> io::Result<()> {
	fs::create_dir_all(destintation)?;
	let file = File::open(source)?;
	let reader = BufReader::new(file);
	let mut archive = Archive::new(reader);

	archive.unpack(destintation)?;

	Ok(())
}

pub fn untar_file_to_ram(path: &str) -> io::Result<HashMap<String, Vec<u8>>> {
	let file = File::open(path)?;
	let mut reader = BufReader::new(file);
	let mut buffer = Vec::new();
	reader.read_to_end(&mut buffer)?;

	untar_to_ram(buffer)
}

pub fn untar_to_ram(bytes: Vec<u8>) -> io::Result<HashMap<String, Vec<u8>>> {
	let cursor = Cursor::new(bytes);
	let mut archive = Archive::new(cursor);
	let mut files = HashMap::new();

	for file in archive.entries()? {
		let mut file = file?;
		let mut contents = Vec::new();
		file.read_to_end(&mut contents)?;

		if let Some(path) = file.path()?.to_str() {
			files.insert(path.to_owned(), contents);
		}
	}
	Ok(files)
}

#[cfg(test)]
mod test {
	use super::*;

	#[test]
	fn test_tar() {
		let res = fs::create_dir_all("tests/results");
		assert!(res.is_ok(), "Could not create test folder");

		let res = tar_folder("tests/pls_compress", "tests/results/test.tar");
		assert!(res.is_ok(), "Error taring");

		let res = untar("tests/test.tar", "tests/results/unpacked/");
		assert!(res.is_ok(), "Error untaring");
	}
}
