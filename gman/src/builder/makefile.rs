use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize)]
pub struct Makefile {
	pub name: String,
	pub version: String,
	pub build_nr: u8,
	pub description: String,
	pub architecture: Vec<String>,
	pub url: String,
	pub license: String,
	pub deps: Vec<String>,
	pub build_deps: Vec<String>,
	pub optional_deps: Vec<String>,
	pub source: String,
	pub sha256sum: String,
	pub update_ver: Vec<String>,
	pub pre_build: Vec<String>,
	pub build: Vec<String>,
	pub package: Vec<String>,
}

impl Default for Makefile {
	fn default() -> Self {
		Makefile {
            name: "gman".to_string(),
            version: "0.1.0".to_string(),
            build_nr: 1,
            description: "A rust based package manager".to_string(),
            architecture: vec!["x86_64".to_string(), "aarch64".to_string()],
            url: "www.example.com".to_string(),
            license: "GPLv3".to_string(),
            deps: vec!["git".to_string(), "zstd".to_string(), "bash".to_string()],
            build_deps: vec!["rust".to_string(), "mold".to_string()],
            optional_deps: vec!["iced".to_string(), "oqs".to_string()],
            source: "git+gitlab.com/Teddy-Kun/gman".to_string(),
            sha256sum: "SKIP".to_string(),
            update_ver: vec![
                "set -o pipefail".to_string(),
                r"git describe --long --abbrev=7 2>/dev/null | sed 's/^[^0-9]\(.*\)/\1/;s/\([^-]*-g\)/r\1/;s/-/./g'".to_string(),
                "printf \"r%s.%s\" \"$(git rev-list --count HEAD)\" \"$(git rev-parse --short=7 HEAD)\"".to_string()    
            ],
            pre_build: vec![
                "".to_string()
            ],
            build: vec![
                "".to_string()
            ],
            package: vec![
                "".to_string()
            ]
        }
	}
}
