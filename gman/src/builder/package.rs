use std::{error::Error, fmt::Display, fs, io};

use super::makefile::Makefile;

#[derive(Debug)]
pub enum MakeErrorKind {
	SrcFmtErr,
	ActionErr,
	FsErr(io::Error),
	GitErr(git2::Error),
}

#[derive(Debug)]
enum SourceAction {
	Git,
}

#[derive(Debug)]
pub struct MakeError {
	kind: MakeErrorKind,
}

impl Display for MakeError {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		match &self.kind {
			MakeErrorKind::SrcFmtErr => write!(f, "source string is incorrectly formatted"),
			MakeErrorKind::ActionErr => write!(f, "undefined source action"),
			MakeErrorKind::FsErr(e) => write!(f, "{}", e),
			MakeErrorKind::GitErr(e) => write!(f, "{}", e),
		}
	}
}

impl Error for MakeError {}

impl From<io::Error> for MakeError {
	fn from(err: io::Error) -> Self {
		MakeError {
			kind: MakeErrorKind::FsErr(err),
		}
	}
}

impl From<git2::Error> for MakeError {
	fn from(err: git2::Error) -> Self {
		MakeError {
			kind: MakeErrorKind::GitErr(err),
		}
	}
}

impl Makefile {
	pub fn gen_file_name(&self) -> String {
		let name = self.name.clone() + "-v" + &self.version + "-" + &self.build_nr.to_string();
		return name;
	}

	pub fn download_source(&self) -> Result<(), MakeError> {
		let surl = self.source.clone();
		let splitted: Vec<&str> = surl.split("+").collect();
		if splitted.len() != 2 {
			return Err(MakeError {
				kind: MakeErrorKind::SrcFmtErr,
			});
		}
		let a = splitted[0];
		let url = splitted[1];
		let action: SourceAction;

		match a {
			"git" => {
				action = SourceAction::Git;
			}
			_ => {
				return Err(MakeError {
					kind: MakeErrorKind::ActionErr,
				})
			}
		}

		match action {
			SourceAction::Git => {
				fs::create_dir("src").map_err(MakeError::from)?;

				git2::Repository::clone(url, "src/.").map_err(MakeError::from)?;
			}
		}

		Ok(())
	}

	pub fn update_version(&self) {}
}
