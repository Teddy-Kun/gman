use std::{error::Error, fmt::Display, fs, io, path::Path};

use config::{Config, ConfigError, File};
use serde::{Deserialize, Serialize};

// We can get any of these errors
#[derive(Debug)]
pub enum ConfErrorKind {
	Io(io::Error),
	Config(ConfigError),
	Toml(toml::ser::Error),
}

#[derive(Debug)]
pub struct ConfError {
	kind: ConfErrorKind,
}

impl Display for ConfError {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		match &self.kind {
			ConfErrorKind::Io(e) => write!(f, "IO Error: {}", e),
			ConfErrorKind::Config(e) => write!(f, "Config Error: {}", e),
			ConfErrorKind::Toml(e) => write!(f, "TOML Error: {}", e),
		}
	}
}

impl Error for ConfError {}

// Convert from 1 of the 3 errors we can get to our custom error type
impl From<io::Error> for ConfError {
	fn from(error: io::Error) -> Self {
		ConfError {
			kind: ConfErrorKind::Io(error),
		}
	}
}

impl From<ConfigError> for ConfError {
	fn from(error: ConfigError) -> Self {
		ConfError {
			kind: ConfErrorKind::Config(error),
		}
	}
}

impl From<toml::ser::Error> for ConfError {
	fn from(error: toml::ser::Error) -> Self {
		ConfError {
			kind: ConfErrorKind::Toml(error),
		}
	}
}

// Initialize the config struct
#[derive(Debug, Deserialize, Serialize)]
pub struct GmanConfig {
	example_int: i64,
	example_string: String,
}

// Default values for the config
impl Default for GmanConfig {
	fn default() -> Self {
		let s = "Hello Config".to_string();
		GmanConfig {
			example_int: 4,
			example_string: s,
		}
	}
}

// load the config file
impl GmanConfig {
	pub fn load(conf_path: &str) -> Result<GmanConfig, ConfError> {
		let path = Path::new(conf_path);

		if !path.exists() {
			create_default_conf(conf_path)?;

			return Ok(GmanConfig::default());
		}

		let builder = Config::builder()
			.add_source(File::with_name(conf_path))
			.build()
			.map_err(ConfError::from)?;

		let conf = builder.try_deserialize().map_err(ConfError::from)?;
		Ok(conf)
	}
}

// create a toml file with the default values at the given path
fn create_default_conf(conf_path: &str) -> Result<(), ConfError> {
	let default_conf = GmanConfig::default();
	let toml = toml::to_string(&default_conf).map_err(ConfError::from)?;

	fs::write(conf_path, toml).map_err(ConfError::from)
}
