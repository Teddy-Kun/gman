use clap::Parser;

/// gman package manager
#[derive(Parser, Debug)]
#[command(author, version)]
pub struct Args {
	/// zstd compression level
	#[arg(long, default_value_t = 3)]
	pub level: u8,

	/// open the iced test gui
	#[arg(short, long)]
	pub gui: bool,

	/// specify the config file path to use
	#[arg(short, long, default_value = "/etc/gman/config.toml")]
	pub config: String,

	/// test gzip decompression
	#[arg(long, default_value = "argument not set")]
	pub gzip: String,

	/// test zstd (de)compression
	#[arg(long, default_value = "argument not set")]
	pub zstd: String,

	/// test (un)taring files
	#[arg(long, default_value = "argument not set")]
	pub tar: String,

	/// test making a makefile
	#[arg(long, default_value = "argument not set")]
	pub make: String,
}

pub fn get_args() -> Args {
	Args::parse()
}
