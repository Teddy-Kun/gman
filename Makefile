debug:
	cargo fetch --locked
	cargo build --frozen

build:
	cargo fetch --locked
	cargo build -r --frozen

optimized:
	cargo fetch --locked
	cargo build --profile optimized --frozen
	
minsize:
	cargo fetch --locked
	cargo build --profile minsize --frozen

all:
	cargo fetch --locked
	cargo build --frozen
	cargo build -r --frozen
	cargo build --profile optimized --frozen
	cargo build --profile minsize --frozen

all-fleet:
	cargo fetch --locked
	fleet build
	fleet build -r
	fleet build --profile optimized
	fleet build --profile minsize
